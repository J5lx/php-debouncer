# PHP Debouncer

A debouncer implementation for PHP to make sure that functions are only called once in a given interval.
