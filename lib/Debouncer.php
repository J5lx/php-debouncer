<?php namespace J5lx\Debouncer;

use Jeremeamia\SuperClosure\SerializableClosure;

class Debouncer
{
    private static $funcData;

    public static function debounce(callable $func, $wait = 0)
    {
        return function () use ($func, $wait) {
            return self::callDebounced($func, $wait);
        };
    }

    public static function callDebounced(callable $func, $wait = 0)
    {
        $serializableFunc = new SerializableClosure($func);
        $funcHash = sha1(serialize($serializableFunc));
        $wait = $wait / 1000; // Convert from microseconds to seconds

        if (!isset(self::$funcData[$funcHash])) {
            self::$funcData[$funcHash] = ['wait' => $wait, 'lastExec' => 0];
        }
        if ($wait != 0) {
            self::$funcData[$funcHash]['wait'] = $wait;
        }

        if ((self::$funcData[$funcHash]['lastExec'] + self::$funcData[$funcHash]['wait']) < microtime(true)) {
            self::$funcData[$funcHash]['lastExec'] = microtime(true);
            return call_user_func($func);
        }
    }
}
